# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


import os.path

import yaml
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
def _load_yaml(yamlf):
    with open(yamlf) as _f:
        return yaml.load(_f, Loader=Loader)

import sdl2cffi as sdl2
from sdl2cffi.api import sdl2_ffi
from sdl2cffi.api import *


__all__ = [
    'TilesetError',
    'Tileset',
]


def _register_yaml_defs(dictupdate, yamlpath):
    dirname = os.path.dirname(yamlpath)
    # TODO: Support zip, tar.gz, etc.
    yamldict = _load_yaml(yamlpath)
    for thingname, thingdata in yamldict.items():
        thingdata['path'] = dirname
        dictupdate[thingname] = thingdata
    
# TODO: Tileset and map namespaces to avoid conflicts?
tilesets = {}
tilesetmaps = {}
def register_tilesets(tilesetspath):
    ''' Add a directory path to load a 'tilesets.yml' from.

    A tileset package is a directory containing at least a "tilesets.yml" file, 
    an image file with the tiles themselves, and optionally a tilesetmaps.yml
    file with a character map file. See the default data/tilesets directory in
    the source distribution for an example.

    '''

    tilesetsyaml_path = os.path.join(tilesetspath, 'tilesets.yml')
    if os.path.exists(tilesetsyaml_path):
        _register_yaml_defs(tilesets, tilesetsyaml_path)

    mapspath = os.path.join(tilesetspath, 'tilesetmaps.yml')
    if os.path.exists(mapspath):
        _register_yaml_defs(tilesetmaps, mapspath)

# Pre-read the tilesets.yml files in the directories under ~/.pyrl/tilesets
_tilesets_path = os.path.join(os.path.expanduser('~'), '.pyrl', 'tilesets')
for dirname in os.listdir(_tilesets_path):
    register_tilesets(os.path.join(_tilesets_path, dirname))


class TilesetError(Exception):
    pass

# TODO: Maybe a TilesetMap class to abstract some of the nitty-gritty of maps?

class Tileset(sdl2.Texture):

    ''' Renders individual tiles from a tileset image. '''

    def __init__(self, tileset, tilesetpath=None,
                 bg=(0,0,0), fg=(255,255,255)):
        ''' Create a new Tileset from the provided image file and parameters.

        * ``tileset``: The name of a tileset as defined in a registered tileset
        package (see register_tilesets()). Tileset packages can contain
        multiple tilesets. By default, tilesets are read from the users local
        PyrlTK config directory (usually $HOME/.pyrl/tilesets on UNIX/Linux
        systems or %APPDATA%\\.pyrl\\tilesets on Windows).

        * ``tilesetpath``: Optionally specify a path to load tilesets from.
        The tilesets defined here will be automatically passed to
        register_tilesets().

        * ``bg``, ``fg``: The background and forground colors for rendering the
        Tileset. Default ``bg`` is black and ``fg`` is white.

        '''

        # TODO: Support "macro" tiles, i.e., tiles which are some multiple of
        # the tile size. Not sure of a good way to do this, maybe with the
        # tilesetnames in the YAML file, or a seperate "macros" section... hmm,
        # the problem with a separate macros section is the line drawing names
        # would be able to be specified there... maybe a list of lists in the
        # "names" section? Maybe even a MetaTile or a generalized Entity class?

        if tilesetpath is not None:
            register_tilesets(tilesetpath)

        # Stash a few instance variables
        tilesetinfo = tilesets[tileset]
        tilesetpath = tilesetinfo['path']
        self.tilesetpath = tilesetpath
        self.name = tileset

        self.bg = bg
        self.fg = fg

        # Extract some info from the tileset definition.
        self.shape = tilesetinfo['shape']
        self.colorkey = tilesetinfo['colorkey']
        self.tileimg  = os.path.join(tilesetpath, tilesetinfo['image'])
        self.mapname = tilesetinfo['map']

        # Load the tilesetmaps.yml
        mapinfo = tilesetmaps[self.mapname]
        if tuple(mapinfo['shape']) != tuple(self.shape):
            raise TilesetError("Map shape for {0} specified by tileset {1} in "
                               "{2} does not match shape defined by tileset "
                               "({3})".format(mapinfo['shape'], tileset,
                                              tilesetpath, self.shape))
        mapfile = os.path.join(mapinfo['path'], self.mapname+'.map')
        self.tilenames = mapinfo['names']
        self.charmap = self._read_charmap(mapfile, self.shape)
        self.tilenames.update(self.charmap)

        # Init the Texture. This just loads the image file, the underlying
        # SDL texture isn't actually initialized until _init_texture is called.
        super(Tileset, self).__init__(
                access=SDL_TEXTUREACCESS_STATIC,
                imgfile=self.tileimg, colorkey=self.colorkey)
        tilew = self.w / self.setw
        tileh = self.h / self.seth
        self.tilesize = (tilew, tileh)

    @property
    def tilesize(self):
        return (self.tilew, self.tileh)

    @tilesize.setter
    def tilesize(self, tilesize):
        self.tilew, self.tileh = tilesize

    @property
    def shape(self):
        return (self.setw, self.seth)

    @shape.setter
    def shape(self, shape):
        self.setw, self.seth = shape

    def _init_texture(self, screen):
        self.screen = screen
        self.sdl_renderer = screen.sdl_renderer

        super(Tileset, self)._init_texture(self.sdl_renderer)

        self.default_bg = sdl2.DrawColor(self.sdl_renderer, self.bg)
        self.default_fg = sdl2.DrawColor(self.sdl_renderer, self.fg)

        # Pre-allocate and populate SDL_Rects for each tile. Memory is cheap
        # and allocating these on the fly can be slow.
        rects = SDL_Rect.array([self.setw, self.seth, 1])
        tilew, tileh = self.tilesize
        for col in range(self.setw):
            rectx = col * tilew
            for row in range(self.seth):
                recty = row * tileh
                rect = rects[col][row][0]
                rect.x = rectx
                rect.y = recty
                rect.w = tilew
                rect.h = tileh
        self._mapping_rects = rects

    def render(self, pos, dst_rect, bg=None, fg=None):
        # See if pos is actually a tile name, and pull it from the names list,
        # otherwise just assume it's a (x,y) pair.
        x, y = self.tilenames.get(pos, pos)
        bg = bg or self.default_bg
        fg = fg or self.default_fg
        with bg, fg:
            self.sdl_renderer.render(self.texture, self._mapping_rects[x][y],
                                     dst_rect)

    @staticmethod
    def _read_charmap(mapfile, shape):
        charmap = {}
        mapw, maph = shape

        with open(mapfile, mode='rb') as _mapfile:
            for rowi in range(maph):
                row = _mapfile.read(mapw)
                if len(row) != mapw:
                    raise TilesetError('Failed to read {0:d} bytes from '
                                       'charmap "{1}".'.format(mapw, mapfile))
                newline = _mapfile.read(1)  # Make sure we have a newline here.
                if newline != b'\n':
                    raise TilesetError('Expected newline at position ({0:d}, '
                                       '{1:d}) in charmap "{2}". Got "{3}".'
                                       .format(rowi, mapw, mapfile, newline))
                for charj, char in enumerate(row):
                    charmap[char] = (charj, rowi)

        return charmap

