# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


import sdl2cffi as sdl2


__all__ = [
    'GridError',
    'Grid',
]


class GridError(Exception):
    pass


class Grid(object):

    ''' The base tile grid.

    XXX

    This presents a dict interface where the keys are 2-tuples representing
    the x, y coordinates of a cell. Values of the cells are 1-character
    strings which will be blitted from the Tileset object to the Screen. All
    values are initialized to the space (' ') character (as opposed to the
    celestial or cosmic Character :P).

    The size of a cell is equal to the default tileset's tile size.

    Note: Grid objects are intended as an abstraction for keeping track of
    off-screen information and have no implicit relationship with what's shown
    on-screen. Use the subclass pyrl.Window for that. They offer all
    of the same functionality but have a defined position on the screen and are
    automatically registered with the Screen's display functions.

    '''

    def __init__(self, size, tileset, fill=' ', bg=(0,0,0), fg=(255,255,255),
                 padding=(0,0)):
        ''' Create a new grid.

        'size': The initial size of the grid as a tuple (width, height) in
        "cells" (tile width/height).

        'tileset': a pyrl.Tileset object. This will be used as the default
        tileset for all characters in the cells. (This can be overridden
        per-character with the various methods below.) Character support is
        entirely governed by the Tileset.

        'fill': The character to fill the grid with when created and when
        clear()'d.

        'bg', 'fg': The background and foreground colors, respectively.

        '''

        self.w, self.h = size
        self.bg, self.fg = bg, fg  # TODO: property?

        # Must be a Tileset object. Window objects support tileset names.
        self.tileset = tileset
        self.filltile = fill

        self.padding = padding

        # TODO Should probably use a numpy structured array, but they don't
        # seem to be working well in pypy just yet...
        self.cells = [[fill] * self.h for _ in xrange(self.w)]
        self.cell_tilesets = [[tileset] * self.h for _ in xrange(self.w)]

        # Don't want to use list comps here because we need distinct DrawColor
        # objects for each cell.
        cell_bgs = []
        cell_fgs = []
        for y in xrange(self.w):
            row_bg = []
            row_fg = []
            cell_bgs.append(row_bg)
            cell_fgs.append(row_fg)
            for x in xrange(self.h):
                row_bg.append(sdl2.DrawColor(tileset.renderer, bg))
                row_fg.append(sdl2.DrawColor(tileset, fg))
        self.cell_bgs = cell_bgs
        self.cell_fgs = cell_fgs

    @property
    def size(self):
        ''' A property returning the (width, height) of the grid in cells.

        Alternatively the width and height are also stored as the attributes
        w and h, respectively.

        '''

        return (self.w, self.h)

    @size.setter
    def size(self, size):
        self.w, self.h = size

    def __getitem__(self, item):
        return self.cells[item[0]][item[1]]

    def __setitem__(self, pos, val):
        # TODO This is going to be done fairly often probably. Might need to
        # profile this and maybe only enable these checks if debugging is on.
        # Could do something like:
        #   self.__setitem__ = self._setitem
        # in __init__ if debugging is disabled...?
        if not (isinstance(pos, tuple) and len(pos) == 2):
            raise GridError('Grid subscripts must be an x,y tuple. '
                               'Got {0}.'.format(repr(pos)))

        if (isinstance(val, str)):
            self.cells[pos[0]][pos[1]] = val
        elif (isinstance(val, int) and val >= 0 and val <= 255):
            self.cells[pos[0]][pos[1]] = chr(val)
        else:
            raise GridError('Grid values must be single-character '
                               'string or int between 0-255 inclusive. '
                               'Got {0}.'.format(repr(val)))

    def __delitem__(self, item):
        self.cells[item[0]][item[1]] = self.filltile

    def __len__(self):
        return self.w * self.h

    def update(self, other):
        ''' Update all cells in the CoordMap from 'other'.

        'other': Can be
            * a dictionary of the from {(x, y): 'c', ...}
            * A 2-D (at least) iterable where each top-level iterable is a
            "row".

        TODO accept arguments describing a rectangle and only update those
        values.

        '''

        if isinstance(other, dict):
            for key, value in other.iteritems():
                self[key] = value
        elif isinstance(other, Iterable):
            for i, row in enumerate(other):
                if isinstance(row, Iterable):
                    for j, val in enumerate(row):
                        self[j,i] = val
                else:
                    raise ValueError('Expected iterable of iterables, got '
                                     'iterable of {0}.'
                                     .format(repr(type(line))))
        else:
            raise ValueError('Unsupported type for CoordMap.update: {0}.'
                             .format(repr(type(other))))

    def fill(self, val, pos=(0,0), size=None, bg=None, fg=None):
        x, y = pos
        if size is None:
            w, h = self.w - x, self.h - y
        else:
            w, h = size

        for i in range(x, x+w):
            for j in range(y, y+h):
                self[i,j] = val
                tileset = self.cell_tilesets[i][j]
                self.setcolor((i,j), bg=bg, fg=fg)

    def blit(self, dest, pos=(0,0), srcpos=(0,0), srcsize=None):
        ''' Copy the cell contents from this CoordMap to the dest CoordMap.

        Cell contents are copied to the dest CoordMap (or subclass).

        'dest': The destination CoordMap to blit to.

        'pos': Optionally specify the upper-left corner of this CoordMap to
        blit. (x, y)

        'srcpos': Optional. The upper-left corner of the area of this CoordMap
        to blit from. (x, y)

        'srcsize': Optional. The size of the area to blit. (w, h)

        '''

        # I've heavily profiled this code trying as many tricks I can think of.
        # If you can find a faster way to do this, please let me know (or send
        # me a pull request on bitbucket! :)

        if srcsize is None:
            src_w = self.w
            src_h = self.h
        else:
            src_w, src_h = srcsize

        src_x, src_y = srcpos
        src_bx = src_x + src_w
        src_by = src_y + src_h
        src_rows = self.cell_tilesets[src_y:src_by]

        dest_x, dest_y = pos
        dest_bx = dest_x + src_w
        dest_by = dest_y + src_h
        dest_rows = dest.cell_tilesets[dest_y:dest_by]

        for src_row, dest_row in itertools.izip(src_rows, dest_rows):
            dest_row[dest_x:dest_bx] = src_row[src_x:src_bx]

    def clear(self, pos=(0,0), size=None):
        ''' Clears the CoordMap to it's initial state.

        Technically this is just a convenience function that calls fill()
        passing in the 'fill' value initially given to __init__ along with the
        pos and size given here (default is to clear everything).

        For pyrl Grids (and most subclasses) the default fill value is ' '
        (the space character).
        '''

        self.fill(self.tileset, pos, size)

    def _setitem(self, pos, val):
        ''' Directly sets the character for a cell, by-passing various checks.

        It's recommended to just use the standard dict item assignment or the
        settile() method unless you know what you're doing. Behavior is
        undefined for poorly formed keys and values. All sanity checks are done
        in __setitem__. You've been warned.

        '''

        super(Grid, self).__setitem__(pos, val)

    def _settileset(self, pos, tileset=None):
        ''' Directly sets the tileset for a cell, by-passing checks.

        It's recommended to use the settileset() or settile() (with the
        optional tileset argument) to change what tileset to use for a cell.

        '''

        self.cell_tilesets[pos[0]][pos[1]] = tileset

    def settile(self, pos, tile, tileset=None, bg=None, fg=None):
        ''' Set the tile for a cell and optionally it's tileset. '''
        # TODO Seems reduntant since we only do something different if a
        # tileset is specified...?
        if tileset is not None:
            if tileset.tilesize != self.tileset.tilesize:
                # TODO Move tileset size checking to Screen and only allow same
                # size tileset for all windows?
                raise GridError('Per-cell tileset must be the same size as '
                                   'the default tileset set for the Grid.')
            self._settileset(pos, tileset)
        else:
            # TODO: Remove all code that sets or accepts a tileset of None.
            #self._settileset(pos, None)
            self._settileset(pos, self.tileset)
        self[pos] = tile
        self.setcolor(pos, bg=bg, fg=fg)

    def setcolor(self, pos, bg=None, fg=None):
        bg = bg or self.bg
        fg = fg or self.fg
        x, y = pos
        tileset = self.cell_tilesets[x][y]
        self.cell_bgs[x][y] = sdl2.DrawColor(tileset.renderer, bg)
        self.cell_fgs[x][y] = sdl2.DrawColor(tileset, fg)

    def prints(self, pos, string, tileset=None, wrap=False):
        # Tileset must have a charmap to print strings
        # TODO Word-wrap?
        # Also, ANSI or some other codes for specifying colors, etc?
        x, y = pos
        maxlen = x + self.w - self.padding[0]
        if not wrap:
            string = string[:maxlen]
        else:
            pass  # TODO
        for i, char in enumerate(string):
            self.settile((x+i, y), char, tileset)

    def drawline(self, startpos, endpos, tile=None, tileset=None,
                 bg=None, fg=None):
        ''' Draws a horizontal or vertical line on the grid.

        startpos and endpos should be (x,y) tuples and should be horizontal or
        vertical from each other.

        The line will be drawn with the given tile. If this is None and the
        tileset supports line-drawing characters, those will be used instead.

        '''

        startx, starty = startpos
        endx, endy = endpos

        if tileset is None:
            tileset = self.tileset

        # TODO Should honor the per-cell tileset...
        if startx == endx:
            # vertical
            if tile is None and 'svl' in tileset.tilenames:
                tile = 'svl'
            for y in xrange(starty, endy):
                self.settile((startx, y), tile, tileset=tileset, bg=bg, fg=fg)

        if starty == endy:
            # horizontal
            if tile is None and 'shl' in tileset.tilenames:
                tile = 'shl'
            for x in xrange(startx, endx):
                self.settile((x, starty), tile, tileset=tileset, bg=bg, fg=fg)

    def drawbox(self, pos, size, tile=None, tileset=None):
        ''' Draws a box using the given tile or line drawing chars if None. '''

        x, y = pos
        w, h = size

        self.drawline(( x+1   , y     ), ( x+w-1 , y     ), tile, tileset)
        self.drawline(( x     , y+1   ), ( x     , y+h-1 ), tile, tileset)
        self.drawline(( x+1   , y+h-1 ), ( x+w-1 , y+h-1 ), tile, tileset)
        self.drawline(( x+w-1 , y+1   ), ( x+w-1 , y+h-1 ), tile, tileset)

        self.settile(  pos,             'sul')
        self.settile(( x+w-1 , y     ), 'sur')
        self.settile(( x     , y+h-1 ), 'sll')
        self.settile(( x+w-1 , y+h-1 ), 'slr')

    def render(self, screen, pos=(0,0)):
        ''' Renders the contents of the grid to the screen. '''

        x, y = pos
        rects = screen.sdl_rects
        cell_bgs = self.cell_bgs
        cell_fgs = self.cell_fgs
        def_tileset = self.tileset
        cell_tilesets = self.cell_tilesets
        for i, row in enumerate(self.cells):
            screen_x = x + i
            for j, tile in enumerate(row):
                screen_y = y + j
                if cell_tilesets[i][j] is not None:
                    tileset = cell_tilesets[i][j]
                else:
                    tileset = def_tileset
                tileset.render(tile, rects[screen_x][screen_y], 
                               cell_bgs[i][j],
                               cell_fgs[i][j])

