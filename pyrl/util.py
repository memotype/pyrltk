# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


import yaml
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


__all__ = [
    'load_yaml',
]


def load_yaml(path):
    # This is a utility function for 2 reasons. 1) So we don't have to import
    # yaml everywhere, and 2) in case we want to add some more path searching
    # logic here, i.e., to search /usr/share/pyrltk or something.
    # TODO: Should probably move to a 'resources' module or something.
    with file(path) as file_:
        return yaml.load(file_, Loader=Loader)

