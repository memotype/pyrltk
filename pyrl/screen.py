# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


from collections import OrderedDict, deque

from sdl2cffi.api import sdl2_ffi
from sdl2cffi.api import *
import sdl2cffi as sdl2

from pyrl.tiles import Tileset


__all__ = [
    'Screen',
]


class Screen(object):
    def __init__(self, size, cellsize, title="PyrlTK",
                 pos=None, sdl_flags=SDL_WINDOW_SHOWN, vsync=True):

        self.title = title
        self.w, self.h = size
        self.cellw, self.cellh = cellsize

        self.windows = OrderedDict()
        self.tilesets = {}

        self.closed = False

        SDL_Init(SDL_INIT_VIDEO)
        SDL_ShowCursor(0)

        self.events = sdl2.Eventerator()

        if pos is None:
            winx, winy = SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED
        else:
            winx, winy = pos

        self.sdl_window = sdl2.Window(title, winx, winy,
                self.w * self.cellw, self.h * self.cellh, sdl_flags)

        if vsync:
            renderer_vsync = SDL_RENDERER_PRESENTVSYNC
        else:
            renderer_vsync = 0
        self.sdl_renderer = sdl2.Renderer(self.sdl_window, -1, renderer_vsync)
        self.sdl_renderer.color = SDL_Color(0, 0, 0, 255)
        self.sdl_renderer.clear()
        self.sdl_renderer.present()

        # Pregenerate SDL_Rects for each cell for performance. Ctypes is slow
        # at creating structures... (Not sure if this is improved since moving
        # to CFFI but I don't feel like rewriting the functions that use rects)
        # TODO Factor out to support resizing...
        self.sdl_rects = SDL_Rect.array([self.w, self.h, 1])
        rects = self.sdl_rects
        cellw, cellh = self.cellw, self.cellh
        winw, winh = self.w, self.h
        for rown in xrange(winh):
            rowp = rown * cellh
            for coln in xrange(winw):
                colp = coln * cellw
                rect = rects[coln][rown][0]
                rect.x = colp
                rect.y = rowp
                rect.w = cellw
                rect.h = cellh

        self.update()

        self.fps_timer = sdl2.Timer(100)

    def quit(self):
        self.closed = True

    def addtileset(self, tileset):
        ''' Register a tileset with the screen.

        If name is None, the name attribute from the Tileset object is used.

        '''

        if len(self.tilesets) == 0:
            self.tilesets['default'] = tileset
        self.tilesets[tileset.name] = tileset
        tileset._init_texture(self)

    def gettileset(self, tileset):
        ''' Get's a tileset from the tileset registry by name.

        If the named tileset doesn't exist, 'tileset' is treated as a file path
        which is passed to loadtileset() with default arguments.

        If a Tileset object is given it's merely returned.

        '''

        if isinstance(tileset, Tileset):
            return tileset
        elif isinstance(tileset, str):
            if tileset in self.tilesets:
                return self.tilesets[tileset]
            else:
                raise ValueError("No tileset with name {0} registered to this "
                                 "screen.".format(tileset))
            # TODO: Broken due to changes in font stuff. Really need a general
            # resource finding module...
            #else:
            #    return self.loadfont(font)

    def register(self, window):
        ''' Register a Window with the screen.

        By default, this is called by the Window class's __init__ method.

        '''

        self.windows[window.title] = window
        self.update()

    def main(self, callback=None):
        ''' Main event loop. Processes events, callbacks, and window refreshes.

        This main loop will dispatch events to callbacks and windows. You can
        provide a callable to the 'callback' parameter which will be called
        with the list of unhandled events as a general fall-back or default
        event handler. The callback will be called even if there are no
        unhandled events (it will be given an empty list) to let the callback
        do other work for each frame if needed. The callback's return value is
        ignored.

        You're welcome to make your own event loop too. Just make sure
        Screen.handle_events and Screen.redraw get called every loop.
        ``handle_events`` will return a list of unhandled events for you to
        process in your loop. ``redraw`` then tells any windows which have said
        they handled an event to redraw their contents if necessary and render
        their tiles to the screen.

        The redraw function also handles framerate limiting.

        '''

        while not self.closed:
            events = self.handle_events()
            if callback is not None:
                callback(events)
            self.redraw()

    def update(self):
        ''' Tell the Screen to do a full render next redraw.

        Windows (or other code) should call this if something was updated
        outside of the Window's redraw or handle_events methods.
        
        '''
        self._need_render = True

    def redraw(self):
        for win in self.windows.itervalues():
            if win.redraw():
                self.update()

        if self._need_render:
            self.sdl_renderer.clear()
            for win in self.windows.itervalues():
                win.render()
            self._need_render = False

            self.present()

        self.avg_fps = self.fps_timer.fps()

    def present(self):
        self.sdl_renderer.present()

    def handle_events(self, events=None):
        ''' Processes the SDL events queue and dispatches key/mouse events to
        windows.

        'events': the list of events from sdl2.ext.get_events(). If 'events' is
        None, this function will call sdl2.ext.get_events().

        Returns a list of events which were not explicitly handled by the
        screen or window objects.

        '''

        # TODO Process global key mapping first

        if events is None:
            events = self.events

        unhandled = []

        for event in events:
            handled = False
            # Windows get events in top-down order
            for win in reversed(self.windows.values()):
                handled = win.handle_event(event)
                if handled:
                    self._need_update = True
                    break
            if not handled:
                unhandled.append(event)

        return unhandled

