# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


from functools import wraps, partial
from collections import defaultdict

#from sdl2 import *
from sdl2cffi.api import *

from pyrl.grid import Grid


__all__ = [
    'Window',
    'keybinding',
]


# A deceptively generic Callback class, but it's used in some tricky decorator,
# metaclass mumbo-jumbo. I'll try to add useful comments where it gets weird.
# TODO: Maybe factor this out to an 'events' module or something...
class Callback(object):
    ''' A callback used for handling keyboard and mouse events.

    While you could directly create Callback objects, it's recommended to just
    use the 'keybinding' decorator below.

    '''

    def __init__(self, callback, keycode, keymod=KMOD_NONE,
                 args=(), kwargs=None):

        # These are read by the _MetaWindow __new__ constructor below to know
        # how to register it to the _keybindings dict on the class being
        # defined.
        self.keycode = keycode
        self.keymod = keymod

        # Save this stuff for later when the call back is actually called.
        self.callback = callback
        self.args = args
        if kwargs is None:
            kwargs = {}
        self.kwargs = kwargs

    def __call__(self, selfself, event):
        # When we're called, all we're given is the event. We need to pass the
        # rest of the args given to the decorator here.
        return self.callback(selfself, event, *self.args, **self.kwargs)

    def __get__(self, instance, instancetype):
        # Needed in order to be able to pass the "self" object that the method
        # is from. For some reason, when the method is wrapped in an object
        # it becomes unbound and doesn't automatically get passed self.

        # TODO This just feels ugly, there has to be a better way...
        return partial(self.__call__, instance)

# Called by the @decorator() notation when the class is read in by the parser.
#pylint: disable-msg=W0613
def keybinding(keycode, keymod=KMOD_NONE, *args, **kwargs):
    ''' A decorator for Window subclasses to register callbacks for events.

    Decorate your Window subclass's methods with this to register them as
    callbacks for specific keycodes. Decorated methods will be called with
    the SDL_Event as the first argument, and any additional args/kwargs given
    to the decorator call.

    'keycode': A SDL2 keycode from the SDL_Keycode enum.
    (http://wiki.libsdl.org/SDL_Keycode)

    'keymod': A SDL2 keymod (mutliple modifiers should be OR'd "|" together).
    This is optional, the default is sdl2.KMOD_NONE.
    http://wiki.libsdl.org/SDL_Keymod

    Additional args and kwargs will be passed directly to the decorated method
    as given.

    For example:

        >>> class MyWindow(Window):
        ...     @keybinding(SDLK_ESCAPE, 42):
        ...     def quit(self, event, num):
        ...         print "The answer is:", num
        ...         self.exit()

    '''

    if kwargs is not None:
        kwargs = kwargs.copy()

    # Called to actually decorate the method and is passed just the method.
    def decorator(func):

        # The thing we return which "replaces" (actually, "wraps") the method
        # itself. The Callback object's __call__ method is what actually gets
        # called when the decorated method is called. Confused yet? I am... :P
        callback = Callback(func, keycode, keymod, args, kwargs)
        return wraps(func)(callback)

    # Return this so it gets called to actually decorate the method.
    return decorator


# Ok, time for some black magic...
class _MetaWindow(type):
    ''' Pay no attention to the metaclass behind the curtain... '''

    # This is called at class definition time. I.e., when the class itself is
    # being created by the interpreter (not when the class is instantiated!)
    def __new__(mcs, name, bases, dct):

        # If not done already, add the _keybindings attr with a freshly
        # allocated defaultdict(list).
        keys = dct.setdefault('_keybindings', defaultdict(list))

        for obj in dct.itervalues():

            # The decorator code above leaves Callback instances in place of
            # the original method. That's how we know what to add it to the
            # _keybindings dict.
            if isinstance(obj, Callback):

                # Add an item to _keybindings to map (keycode, keymod) tuples
                # to a list of Callback objects (in case there are more).
                keys[obj.keycode, obj.keymod].append(obj)

        # Construct the class and return it. (super() works just the same here,
        # basically calls type.__new__().
        return super(_MetaWindow, mcs).__new__(mcs, name, bases, dct)

# WHEW. All that just to have a simple callback decorator. :)
# The rest of this is just pretty basic python code... The callbacks are used
# in the Window.handle_events() method below.

class Window(Grid):

    ''' A general/base window class.

    A Grid with a defined position on the Screen and various common features
    of a Window, such as borders, callbacks for UI elements, etc.

    '''

    __metaclass__ = _MetaWindow
    _keybindings = {}

    def __init__(self, screen, pos, size, title=None, tileset=None,
                 border=False, **kwargs):
        ''' Create a new Window on the specified Screen.

        New windows automatically register themselves with the provided screen
        and are rendered every frame.

        'screen': The screen object this window is displayed on.

        'title': A title for the window. This will be used to identify the
        window in the screen. If the border is enabled it will also be printed
        at the top of the border as a window title.

        'size': A tuple representing the (width, height) of the window in
        characters/cells.

        'pos': The position of the window (in characters/cells). Defaults to
        (0, 0).

        'tileset': The default Tileset object for the window. Optional. If
        None, will use the default Tileset for the Screen.

        'border': A boolean for whether or not the Window should draw a border
        around itself. If true, the 'title' will be printed at the top. The
        border is drawn within the window, so adjust your calculations.

        '**kwargs': Any additional keyword arguments are passed to the Grid
        constructor.

        '''

        # TODO Add "centered" options...
        # TODO Need resize() method...

        self.screen = screen
        self.title = title
        self.x, self.y = pos
        if tileset is None:
            tileset = screen.tilesets['default']
        self._border = border

        # Be sure to render at least once at the beginning.
        self._need_update = True

        if border:
            # TODO doc
            padding = (1,1)
        else:
            padding = (0,0)

        super(Window, self).__init__(size, screen.gettileset(tileset),
                                     padding=padding, **kwargs)

        screen.register(self)

    @property
    def pos(self):
        ''' (width, height) '''
        return (self.x, self.y)

    @pos.setter
    def pos(self, pos):
        self.x, self.y = pos
        self._need_update = True

    def redraw(self):
        ''' Called every frame to give the window a chance to make updates.

        Subclasses should extend this method to handle any per-frame updates.

        '''

        if self._border:
            self._redraw_border()
        return True

    def _redraw_border(self):
        self.drawbox((0,0), self.size)
        self.prints((1,0), self.title[:self.w-2])

    # pylint: disable-msg=W0221
    def render(self):
        ''' Called each frame to blit the actual tiles to the screen. '''
        super(Window, self).render(self.screen, self.pos)

    def shift(self, amount):
        ''' Shifts the window by the specified offsets.

        'amount': A tuple of x and y offsets.

        '''

        newx = (self.x + amount[0]) % (self.screen.w - self.w + 1)
        newy = (self.y + amount[1]) % (self.screen.h - self.h + 1)
        self.pos = (newx, newy)

    def addcallback(self, callback, keycode=None, keymod=None,
                    args=(), kwargs=None):
        ''' Register a callback for this window. '''

        if isinstance(callback, Callback):
            if keycode is None:
                keycode = callback.keycode
            if keymod is None:
                keymod = callback.keymod
        else:
            if keymod is None:
                keymod = KMOD_NONE
            callback = Callback(callback, keycode, keymod, args, kwargs)
        self._keybindings[keycode, keymod].append(callback)

    def handle_event(self, event):
        ''' Called before each frame to handle any pending events.

        This method triggers any registered callbacks. The first callback to
        return True is considered to have handled the event and this method
        will return True.

        Subclasses should be sure to call the parent method if they plan to
        support callbacks.

        Return True to trigger a redraw call before rendering the frame.

        '''

        keymod = event.key.keysym.mod
        keycode = event.key.keysym.sym

        # And here is where the fruit of our decorator/metaclass mess pays off.

        # _keybindings is set by either addkey() or by the metaclass if the
        # subclass has any Callback type attributes (possibly created by the
        # keybinding() decorator
        if not hasattr(self, '_keybindings'):
            return False

        for callback in self._keybindings.get((keycode, keymod), []):
            if callback(self, event):
                return True

