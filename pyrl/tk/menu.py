# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


from pyrl.tk.window import Window


__all__ = [
    'Menu',
]


class Menu(Window):
    def __init__(self, screen, pos, size, title=None, items=(), **kwargs):
        ''' Create a Menu on the screen.

        ``screen``: The Screen object this menu will render too.

        ``pos``: The absolut position of the upper-left corner of the menu.

        ``size``: Can either be a (w, h) tuple, or just an integer representing
        the number of menu items to display at a time. The title, if given,
        will be added to the height.

        ``items``: A list of menu items, each item being a tuple of the form
        ("Menu text", callback).

        ``**kwargs``: Any other keyword arguments to be passed to the Window
        __init__ method.

        '''

        if isinstance(size, int):
            size = (size, len(items))

        self._items = []
        self._callbacks = {}

        for item, callback in items:
            self.additem(item, callback=callback)

        super(Menu, self).__init__(screen, pos, size, title, **kwargs)

    def additem(self, item, pos=None, callback=None):
        ''' Add an item to the menu.

        ``item``: The text displayed for this item, or a MenuItem object.

        ``pos``: An integer indicating where the item should be inserted in the
        menu list. Default is to append the item to the end of the list.

        ``callback``: A callback to be called if the menu item is chosen.

        '''

        if pos is not None:
            self._items.insert(pos, item)
        else:
            self._items.append(item)

        self._callbacks[item] = callback

    def redraw(self):
        ''' Clears the characters in the window and regenerates them from the
        menu items.

        This will undo any direct changes to the character map for this window.

        '''

        xpad, ypad = self.padding

        for y, item in enumerate(self._items):
            self.prints((xpad, y + ypad), item)

        super(Menu, self).redraw()

        return True


