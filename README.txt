PyRL-TK - A Python RogueLike Took-Kit

Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
All rights reserved.

Licensed under Apache License, Version 2.0.
See LICENSE.txt for licensing details.
