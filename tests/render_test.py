# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


import os.path
import sys

import sdl2cffi as sdl2
from sdl2cffi.api import *

import pyrl
from pyrl import keybinding
from pyrl import tk

def p(t='\n'):
    print(t),
    sys.stdout.flush()

fontpath = './data/tiles'
fontname = 'cp437_16x16'
fontname2 = 'terminal16x16'

fontinfo = pyrl.tiles.tilesets[fontname]

#font = screen.loadtileset(fontpath, fontfile)
#font2 = screen.loadtileset(fontpath, fontfile2)
font  = pyrl.tiles.Tileset(fontname)
font2 = pyrl.tiles.Tileset(fontname2)

#screen = pyrl.Screen('ROLE Test', (60, 40), (16, 16))
screen = pyrl.Screen((60,40), font.tilesize, title='ROLE Test')
screen.addtileset(font)
screen.addtileset(font2)

p('Window.__init__')
sys.stdout.flush()
win = tk.Window(screen, (0,0), (16,16), 'test window', tileset=fontname)
p('done\n')

p('creating u')
u = dict(((y, x) for x, y in font.charmap.iteritems()))
ukeys = sorted(u.keys())
p('done\n')

p('win.update(u)')
win.update(u)
p('done\n')
win.settile((15,0), '?')

p('win2 init')
win2 = tk.Window(screen, (16,0), (16,16), 'test window2', 
                   tileset='terminal16x16')
p('done\n')
win2.update(u)
#win2.prints((0, 1), "Hello again!")

class MyWindow(tk.Window):
    count = 0
    def handle_event(self, event):
        keytype = event.key.type
        keymod = event.key.keysym.mod
        keysym = event.key.keysym.sym

        if keytype == SDL_KEYDOWN:
            if keysym == SDLK_DOWN:
                self.shift((0, 1))
                return True
            if keysym == SDLK_UP:
                self.shift((0, -1))
                return True
            if keysym == SDLK_LEFT:
                self.shift((-1, 0))
                return True
            if keysym == SDLK_RIGHT:
                self.shift((1, 0))
                return True

            if keysym in range(SDLK_0, SDLK_9):
                self.count *= 10
                self.count += keysym - SDLK_0
                print(self.count)
                return True
            elif self.count > 0:
                for i in xrange(self.count):
                    # TODO Event handling seems kind of slow with 100 or so
                    # events in the queue... maybe need a better way to do
                    # this...
                    SDL_PushEvent(event)
                self.count = 0
                return True

        return super(MyWindow, self).handle_event(event)
    
    quux = 123
    @keybinding(SDLK_SPACE, args=(42,), kwargs={'foo': 'bar'})
    def quit(self, event, num, foo='baz'):
        # TODO: Better way to quit out of event loop gracefully... Maybe an 
        # exception? Maybe StopIteration?
        #print("quit callback called")
        #print(repr(args), repr(kwargs))
        print(event.key.keysym.sym, repr(num), repr(foo), self.quux)
        return True


p('win3 init')
win3 = MyWindow(screen, (16,16), (10,10), 'test window3', 
                tileset='terminal16x16', fill='#', border=True)
p('done\n')
p('win3.fill')
win3.fill('#', size=(5,5))
win3.fill('@', pos=(5,5), size=(5,5), bg=(0,0,0), fg=(255,0,0))
p('done\n')

menu1_items = [
    ('New game', None),
    ('Load game', None),
    ('Options', None),
]

menu1 = tk.Menu(screen, (30, 30), (20, 4), "Main Menu", menu1_items,
                     border=True)

class Game(object):
    def __init__(self, screen):
        self.screen = screen
        self.last_ticks = SDL_GetTicks()
        self.fps = [self.last_ticks] * 10
        self.framen = 0

        self.running = True

    def main(self):
        screen.main(self.default_events)

    def default_events(self, events):
        for event in events:
            if event.type == SDL_QUIT:
                p('default_events, SDL_QUIT')
                self.screen.quit()
                break
            if event.type == sdl2.SDL_KEYDOWN:
                if event.key.keysym.sym == SDLK_ESCAPE:
                    p('default_events, SDLK_ESCAPE')
                    self.screen.quit()
                    break

        #self.screen.redraw()

        #ticks = SDL_GetTicks()
        #frame_ticks = ticks - self.last_ticks
        #frame_sec = (frame_ticks / 1000.0) + 0.0001

        #self.frame_fps = 1.0 / frame_sec
        #self.fps.pop(0)
        #self.fps.append(self.frame_fps)
        #self.avg_fps = sum(self.fps)/len(self.fps)
        ##print(repr(fps))
        #print("Last ticks {0:d} / Frame sec {1:f} / FPS {2:f} / Avg FPS {3:f}"\
        #    .format(self.last_ticks, frame_sec, self.frame_fps, self.avg_fps))
        #self.last_ticks = SDL_GetTicks()

        self.framen += 1
        if self.framen > 100:
            print("Avg FPS {0:f}".format(self.screen.avg_fps))
            self.framen = 0


def main():
    p('Entering main()\n')
    game = Game(screen)
    game.main()
    p('Exiting main()\n')


if __name__ == '__main__': main()
