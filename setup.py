# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# The full license is also available in the file LICENSE.apache-2.0.txt


import os
from setuptools import setup, find_packages


# Packages can't have periods anyway, and setuptools expects UNIX-style
# directory seperation, so assuming all periods in find_packages() are
# directory seperators should be safe. Don't give me that purist crap! :P
pkgdata = {}
for pkg in find_packages():
    pkgdir = './' + pkg.replace('.', '/') 
    ls = os.listdir(pkgdir)
    if 'data' in ls:
        pkgdata_files = os.listdir('/'.join([pkgdir, 'data']))
        pkgdata[pkg] = ['data/' + f for f in pkgdata_files]

setup(
    name = 'pyrl',
    version = '0.1',
    description = 'A Python Roguelike library using SDL2',
    author = 'Isaac Freeman',
    author_email = 'memotype@gmail.com',
    url = 'https://bitbucket.org/memotype/pyrl',

    packages = find_packages(),
    package_data = pkgdata,
    include_package_data = True,

    install_requires = ['sdl2cffi', 'PyYAML'],
    dependency_links = [
        'git+https://bitbucket.org/memotype/sdl2cffi.git#egg=sdl2cffi'
    ]
) 
