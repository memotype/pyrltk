# source me

if ! python -c "import virtualenvwrapper"; then
    echo
    echo "!!! Current python env doesn't have virtualenvwrapper"
    echo "!!! Please manually run deactivate or install it in your current env"
    echo
    return
fi

python_exec="$( which python )"
old_venv="$( basename "$VIRTUAL_ENV" )"

[[ -n $old_venv ]] && deactivate

venv="venv-$RANDOM"
mkvirtualenv "$venv" -p "$python_exec"
workon "$venv"
env
python setup.py install
pushd /tmp  # Make sure we're not just importing from PWD
if python -c "import pyrl"; then
    echo
    echo "+++ Import succeeded."
    echo
else
    echo
    echo "!!! Import failed!"
    echo
fi
popd
deactivate
rmvirtualenv "$venv"

[[ -n $old_venv ]] && workon "$old_venv"
